MODULE jdwm_fort
  IMPLICIT NONE
  
CONTAINS

  SUBROUTINE cumsum(x, N, out)
    IMPLICIT NONE
    INTEGER, PARAMETER :: MK = KIND(1.0D0)
    INTEGER, INTENT(IN) :: N
    REAL(MK), INTENT(IN) :: x(N)
    REAL(MK), INTENT(OUT) :: out(N)
    
    REAL(MK) :: prev
    INTEGER :: i
    
    DO i = 1, N
      IF (i == 1) THEN
        prev = 0.0
      ELSE
        prev = out(i-1)
      END IF
      out(i) = x(i) + prev
    END DO
  END SUBROUTINE cumsum



  SUBROUTINE cumtrapz(y, dx, N, out)
    IMPLICIT NONE
    INTEGER, PARAMETER :: MK = KIND(1.0D0)
    INTEGER, INTENT(IN) :: N
    REAL(MK), INTENT(IN) :: y(N), dx
    REAL(MK), INTENT(OUT) :: out(N-1)
    
    CALL cumsum(dx * (y(2:) + y(:N-1)) / 2, N-1, out)
    
  END SUBROUTINE cumtrapz



  SUBROUTINE calculate_radial_velocity(r, U_m, U_p, dr, dx, Nr, V_out)
    IMPLICIT NONE
    INTEGER, PARAMETER :: MK = KIND(1.0D0)
    INTEGER, INTENT(IN) :: Nr
    REAL(MK), INTENT(IN) :: r(Nr), U_m(Nr), U_p(Nr)
    REAL(MK), INTENT(IN) :: dr, dx
    REAL(MK), INTENT(OUT) :: V_out(Nr)
    
    REAL(MK) :: dudx(Nr), midpoints(Nr-1)
    INTEGER :: i
    
    dudx = (U_p - U_m) / dx
    CALL cumtrapz(r * dudx, dr, Nr, V_out(2:))
    V_out(2:) = -V_out(2:) / r(2:)
    
  END SUBROUTINE calculate_radial_velocity
    
    
    
  SUBROUTINE make_transition_matrix(r, U, V, visc, dx, Nr, ab, B)
    IMPLICIT NONE
    INTEGER, PARAMETER :: MK = KIND(1.0D0)
    INTEGER, INTENT(IN) :: Nr
    REAL(MK), INTENT(IN) :: r(Nr), U(Nr), V(Nr), visc(Nr)
    REAL(MK), INTENT(IN) :: dx
    REAL(MK), INTENT(OUT) :: ab(3, Nr)
    REAL(MK), INTENT(OUT) :: B(Nr)
    
    REAL(MK) :: dr
    REAL(MK) :: V_m(Nr-1), V_(Nr), V_p(Nr-1)
    REAL(MK) :: a(Nr), d(Nr)
    
    V_m = 0.0 
    V_ = 0.0 
    V_p = 0.0 
    
    dr = r(2) - r(1)
    
    a = visc / dr ** 2
    d(2 : Nr) = visc(2 : Nr) / (2 * r(2 : Nr) * dr) - V(2 : Nr) / (2 * dr)
    
    V_m(1:Nr-1) = V_m(1:Nr-1) + d(2 : Nr)
    V_m(1:Nr-1) = V_m(1:Nr-1) + -a(2 : Nr)
     
    V_(2:Nr-1) = V_(2:Nr-1) + U(2 : Nr) / dx
    V_(2:Nr-1) = V_(2:Nr-1) + 2 * a(2 : Nr)
     
    V_p(2:Nr-1) = V_p(2:Nr-1) - d(2 : Nr)
    V_p(2:Nr-1) = V_p(2:Nr-1) + -a(2 : Nr)
     
    ! wake center boundary conditions
    V_(1) = U(1) / dx + 2 * a(1)
    V_p(1) = -2 * a(1)
    
    ! wake edge boundary conditions
    V_m(Nr - 1) = 0.0
    V_(Nr) = 1.0 / dx
     
    ! Store the A matrix in banded structure compatible with LAPACK.
    ab = 0.0
    ab(1, 2:Nr) = V_p
    ab(2, :) = V_
    ab(3, 1:Nr-1) = V_m
    ! 
    B = 0.0
    B(1:Nr-1) = U(1:Nr-1) ** 2 / dx
    B(Nr) = U(Nr) / dx
    
  END SUBROUTINE make_transition_matrix


  SUBROUTINE evolve_explicit(r, U, V, visc, dx, Nr, Uout, Vout)
    IMPLICIT NONE
    INTEGER, PARAMETER :: MK = KIND(1.0D0)
    INTEGER, INTENT(IN) :: Nr
    REAL(MK), INTENT(IN) :: r(Nr), U(Nr), V(Nr), visc(Nr)
    REAL(MK), INTENT(IN) :: dx
    REAL(MK), INTENT(OUT) :: Uout(Nr), Vout(Nr)
    
    REAL(MK) :: dr
    REAL(MK) :: a(Nr), d(Nr)
    
    dr = r(2) - r(1)

    Uout = 0.0
    a = 0.0
    d = 0.0
    ! 
    a = visc / dr ** 2
    d(2:Nr-1) = visc(2:Nr-1) / (2 * r(2:Nr-1) * dr) - V(2:Nr-1) / (2 * dr)
    ! 
    Uout(2:Nr-1) = U(2:Nr-1)
    Uout(2:Nr-1) = Uout(2:Nr-1) + dx * (-2 * a(2:Nr-1))
    Uout(2:Nr-1) = Uout(2:Nr-1) + dx * (U(3:) / U(2:Nr-1) * (a(2:Nr-1) + d(2:Nr-1)))
    Uout(2:Nr-1) = Uout(2:Nr-1) + dx * (U(:Nr-2) / U(2:Nr-1) * (a(2:Nr-1) - d(2:Nr-1)))
    
    ! Boundary condition at center (mirrored)
    Uout(1) = U(1) + 2 * dx * a(1) * (U(2) / U(1) - 1)
    
    ! Boundary condition at large radius (fixed)
    Uout(Nr) = U(Nr)
    CALL calculate_radial_velocity(r, U, Uout, dr, dx, Nr, Vout)
  END SUBROUTINE evolve_explicit
  
  
  SUBROUTINE wake_width(r, U, N, out)
    IMPLICIT NONE
    INTEGER, PARAMETER :: MK = KIND(1.0D0)

    INTEGER, INTENT(IN) :: N
    REAL(MK), INTENT(IN) :: r(N), U(N)
    REAL(MK), INTENT(OUT) :: out
    
    REAL(MK) :: dr(N-1), r_mid(N-1), dA(N-1), cumulative_def(N-1)
    REAL(MK) :: total_def
    INTEGER :: i
    
    dr = r(2:) - r(:N-1)
    r_mid = r(:N-1) + dr / 2
    dA =  r(2:) ** 2 - r(:N-1) ** 2 ! usually multiplied by pi, but it isn't necessary here.
    
    total_def = SUM((1 - U(2:)) * dA)
    CALL cumsum((1 - U(2:)) * dA, N - 1, cumulative_def)
    
    IF (total_def == 0) THEN
        out = 0
        
    ELSE
      cumulative_def = cumulative_def / total_def
      DO i=1, N-1
        IF (cumulative_def(i) >= 0.95) THEN
          EXIT
        END IF
      END DO
      
  
      IF (i == N-1) THEN
        out = 0
      ELSE
        out = r_mid(i-1)
      ENDIF
    
    END IF

  END SUBROUTINE wake_width
END MODULE jdwm_fort
