import numpy as np
from jDWM import Ainslie
from jDWM import fAinslie
from jDWM import utilities
from jDWM.fortran import jdwm_fort
from scipy.stats import norm
from tqdm import tqdm, trange
import time

rng = np.random.default_rng(0)


if __name__ == "__main__":
    N = 100
    N_tests = 100000
    r = np.linspace(0, 3, 100)
    dr = r[1] - r[0]
    dx = 0.1
    U_input = rng.normal(size=(N_tests, N))
    U2_input = rng.normal(size=(N_tests, N))
    V_input = rng.normal(size=(N_tests, N))
    visc_input = rng.normal(size=(N_tests, N))

    print("testing make_transition_matrix...")
    t1 = time.time()
    for U, V, visc in zip(tqdm(U_input, desc="Python"), V_input, visc_input):
        ab1, B1 = Ainslie.make_transition_matrix(r, U, V, visc, dx)

    t2 = time.time()
    for U, V, visc in zip(tqdm(U_input, desc="Fortran"), V_input, visc_input):
        ab1, B1 = fAinslie.make_transition_matrix(r, U, V, visc, dx)
    t3 = time.time()
    print(f"{(t2-t1)/(t3-t2):2.2f}x speed up.")

    print("testing calculate_radial_velocity...")
    t1 = time.time()
    for U, U2 in zip(tqdm(U_input, desc="Python"), U2_input):
        V = Ainslie.calculate_radial_velocity(r, U, U2, dr, dx)

    t2 = time.time()
    for U, U2 in zip(tqdm(U_input, desc="Fortran"), U2_input):
        V = fAinslie.calculate_radial_velocity(r, U, U2, dr, dx)
    t3 = time.time()
    print(f"{(t2-t1)/(t3-t2):2.2f}x speed up.")

    N = 501
    N_tests = 10000
    r = np.linspace(0, 3, N)
    scale = rng.uniform(0, 5, size=N_tests)
    X = np.zeros((N_tests, N))
    widths1 = np.zeros(N_tests)
    widths2 = np.zeros(N_tests)
    for i in range(N_tests):
        X[i, :] = 1 - norm.pdf(r, scale=scale[i])

    print("testing wake_width...")
    t1 = time.time()
    for i, x in enumerate(tqdm(X, desc="Python")):
        widths1[i] = utilities.wake_width(r, x)

    t2 = time.time()
    for i, x in enumerate(tqdm(X, desc="Fortran")):
        widths2[i] = jdwm_fort.wake_width(r, x, N)
    t3 = time.time()
    print(f"{(t2-t1)/(t3-t2):2.2f}x speed up.")
    np.testing.assert_array_almost_equal(widths1, widths2)
