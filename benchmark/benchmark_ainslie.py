from jDWM import Ainslie, AxialInductionModel, BoundaryCondition, EddyViscosityModel
import numpy as np
import profilehooks
from tqdm import trange


def fixture():
    r = np.linspace(0, 3, 501)
    axial_induction_model = AxialInductionModel.Joukowsky()
    boundary_condition = BoundaryCondition.madsen()
    viscosity_model = EddyViscosityModel.madsen()

    r, a = axial_induction_model.function()
    U, V = boundary_condition.function(r, a)

    visc = viscosity_model.function(0.01, r, U)

    return r, U, V, visc


@profilehooks.timecall(immediate=False)
@profilehooks.profile(filename="benchmark_ainslie.prof")
def call_ainslie(r, U, V, visc):
    return Ainslie.evolve(r, U, V, visc, 0.01, r[1] - r[0])


if __name__ == "__main__":

    r, U, V, visc = fixture()
    N = 4000
    for i in trange(N):
        _U, _V = call_ainslie(r, U, V, visc)
