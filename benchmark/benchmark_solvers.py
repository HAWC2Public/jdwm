import time
import numpy as np
from tqdm import tqdm, trange
from jDWM.Wake import StaticWake

rng = np.random.default_rng(0)

solver_names = ["python", "implicit", "explicit"]
params = {
    "tsr": 7,
    "ct": 0.5,
    "TI": 0.1,
    "Nr": 51,
}
if __name__ == "__main__":

    N_tests = 1000

    print("testing solvers...")
    for name in solver_names:
        dwm = StaticWake(solver=name, **params)
        for i in trange(N_tests, desc=name):
            dwm.solve()
