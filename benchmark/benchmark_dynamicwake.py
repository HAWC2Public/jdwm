import numpy as np
from jDWM.Wake import DynamicWake
import profilehooks


@profilehooks.profile(filename="benchmark_dynamicwake.prof")
def make_step(wake, ct):
    wake.step(dt=0.1, ct=ct)


if __name__ == "__main__":
    wake = DynamicWake(U=0.1)
    for i in range(50):
        ct = 0.2 + 0.75 * (1 - np.sin(i / 500 * np.pi))
        make_step(wake, ct)
