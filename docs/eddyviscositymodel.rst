
Eddy Viscosity Models
=====================================

.. plot:: plots/viscosity_models.py


.. automodule:: jDWM.EddyViscosityModel
    :members:
    :undoc-members:
