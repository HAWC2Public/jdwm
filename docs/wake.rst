
Wake Objects
=====================================


.. autoclass:: jDWM.Wake.StaticWake
    :members:
    :undoc-members:
    
    
    
.. autoclass:: jDWM.Wake.DynamicWake
    :members:
    :undoc-members:
