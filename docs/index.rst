.. jDWM documentation master file, created by
   sphinx-quickstart on Thu Dec  5 16:13:41 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to jDWM's documentation!
================================

.. toctree::
   :maxdepth: 1
   :name: mastertoc
   
   wake
   axialinductionmodel
   eddyviscositymodel
   filterfunctions
   utilities
   boundarycondition


Indices and tables
==================

* :ref:`genindex`
