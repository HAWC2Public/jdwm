
Filter Functions
=====================================
.. plot:: plots/filter_functions.py


.. automodule:: jDWM.FilterFunctions
    :members:
    :undoc-members:
