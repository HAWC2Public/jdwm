from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
from jDWM.Wake import StaticWake

fig_dir = Path(__file__).parent.absolute().parent / "fig"


def main():
    if __name__ == "__main__":
        dwm = StaticWake(tsr=7, ct=0.5, TI=0.1)
        y, x, U, V, widths, dUdr = dwm.solve()

        # plot axial induction distribution
        fig = plt.figure(dpi=200)
        plt.title("Longitudinal wind speed")
        plt.imshow(
            U.T,
            extent=[min(x), max(x), min(y), max(y)],
            origin="lower",
            interpolation="bilinear",
        )
        plt.imshow(
            U.T,
            extent=[min(x), max(x), min(y), -max(y)],
            origin="lower",
            interpolation="bilinear",
        )

        plt.plot(x, widths, "r--", lw=0.5)
        plt.plot(x, -widths, "r--", lw=0.5)

        plt.xlabel("$x/R$")
        plt.ylabel("$r/R$")

        plt.tight_layout()
        plt.savefig(fig_dir / "002_wake_U.png", dpi=200, bbox_inches=0)

        # plot radial induction distribution
        fig = plt.figure(dpi=200)
        plt.title("Longitudinal wind speed")
        plt.imshow(
            V.T,
            extent=[min(x), max(x), min(y), max(y)],
            origin="lower",
            interpolation="bilinear",
        )
        plt.imshow(
            V.T,
            extent=[min(x), max(x), min(y), -max(y)],
            origin="lower",
            interpolation="bilinear",
        )

        plt.plot(x, widths, "r--", lw=0.5)
        plt.plot(x, -widths, "r--", lw=0.5)

        plt.xlabel("$x/R$")
        plt.ylabel("$r/R$")

        plt.tight_layout()
        plt.savefig(fig_dir / "002_wake_V.png", dpi=200, bbox_inches=0)
        plt.show()
main()
