from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
from jDWM.Wake import StaticWake

fig_dir = Path(__file__).parent.absolute().parent / "fig"


def main():
    if __name__ == '__main__':

        visc_model_names = ["madsen", "larsen", "IEC", "keck"]

        params = {
            "tsr": 7,
            "ct": 0.5,
            "TI": 0.1,
        }
        dwms = [
            StaticWake(viscosity_model=x, boundary_model="none", **params)
            for x in visc_model_names
        ]
        res = [x.solve() for x in dwms]

        N = len(dwms)

        fig, axes = plt.subplots(1, N, sharey=True)
        axes[0].set_ylabel("$x/R$")
        for i, ((x, y, U, V, widths, dUdr), dwm) in enumerate(zip(res, dwms)):
            axes[i].set_title(visc_model_names[i])
            axes[i].imshow(
                U,
                extent=[min(x), max(x), min(y), max(y)],
                origin="lower",
                interpolation="bilinear",
            )
            axes[i].imshow(
                U,
                extent=[min(x), -max(x), min(y), max(y)],
                origin="lower",
                interpolation="bilinear",
            )

            axes[i].plot(widths, y, "r--", lw=0.5)
            axes[i].plot(-widths, y, "r--", lw=0.5)

            axes[i].set_xlabel("$r/R$")

        plt.tight_layout()
        plt.savefig(fig_dir / "005_viscosity_models.png", dpi=200, bbox_inches="tight")
        plt.show()
main()
