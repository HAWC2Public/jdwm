from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
from jDWM import FilterFunctions

fig_dir = Path(__file__).parent.absolute().parent / "fig"


def main():
    if __name__ == '__main__':

        filterfunctions = {
            "Madsen": FilterFunctions.madsen(),
            "Larsen": FilterFunctions.larsen(),
            "Keck": FilterFunctions.keck(),
            "IEC": FilterFunctions.IEC(),
        }

        linestyles = {
            "Madsen": "-",
            "Larsen": "-",
            "Keck": "--",
            "IEC": "--k",
        }
        x = np.linspace(0, 40)

        fig, axes = plt.subplots(2, 1, sharex=True)
        axes[0].set_title("Filter functions")
        axes[1].set_xlabel("Downstream distance, x/R")
        axes[0].set_ylabel("F1")
        axes[1].set_ylabel("F2")
        for name, ff in filterfunctions.items():
            F1 = [ff.filter1(_x) for _x in x]
            axes[0].plot(x, F1, linestyles[name], label=name)

            F2 = [ff.filter2(_x) for _x in x]
            axes[1].plot(x, F2, linestyles[name], label=name)

        axes[1].legend()
        plt.tight_layout()
        plt.savefig(fig_dir / "004_filter_functions.png", dpi=200, bbox_inches="tight")
        plt.show()
main()
