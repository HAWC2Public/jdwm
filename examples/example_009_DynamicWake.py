from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
from jDWM.Wake import DynamicWake
from tqdm import trange

fig_dir = Path(__file__).parent.absolute().parent / "fig"

RESOLUTION = 100
rng = np.random.default_rng(12345)
N = 100
params = {
    "Nr": 21,
    "d_particle": 0.1,
    "dt": 0.5,
    "fc": 1,
    "tsr": 7,
    "a_target": 0.333,
    "axial_induction_model": "InductionMatch2",
    "x0": 0,
    "y0": 0,
    "z0": 0,
    "d_particle": 1,
    "TI": 0.1,
    "max_particles": 20,
    "solver": "implicit",
}


def wsp_func(x, y, z):
    return 1, 0.2 * rng.standard_normal(), 0.2 * rng.standard_normal()


def plot_wake(wake):
    xmin = -5
    xmax = 30
    ymin = -5
    ymax = 5

    Xs = np.linspace(xmin, xmax, RESOLUTION)
    Ys = np.linspace(ymin, ymax, RESOLUTION)
    Zs = [0]

    Xmesh, Ymesh, Zmesh = np.meshgrid(Xs, Ys, Zs, indexing="ij")
    U1 = wake.wsp(Xmesh, Ymesh, Zmesh)

    plt.figure()

    plt.imshow(
        U1[:, :, 0].T,
        extent=(xmin, xmax, ymin, ymax),
        cmap="RdYlBu_r",
        interpolation="bicubic",
        origin="lower",
    )

    wake_x = [p.x for p in wake.particles]
    wake_y = [p.y for p in wake.particles]
    plt.plot(wake_x, wake_y, ".--b", lw=1, alpha=0.2, ms=1)

    plt.xlim(xmin, xmax)
    plt.ylim(ymin, ymax)
    plt.savefig(fig_dir / "009_DynamicWake.png", dpi=200, bbox_inches="tight")


def main():

    if __name__ == "__main__":
        wake = DynamicWake(**params)
        for i in trange(500):
            positions = [(p.x, p.y, p.z) for p in wake.particles]
            wsp_list = [wsp_func(x, y, z) for (x, y, z) in positions]
            wake.step(wsp_list)

        plot_wake(wake)
        plt.show()
main()
