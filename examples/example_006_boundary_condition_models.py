from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
from jDWM import AxialInductionModel, BoundaryCondition, Wake

fig_dir = Path(__file__).parent.absolute().parent / "fig"


def main():
    if __name__ == '__main__':

        r_max = 3
        r = np.linspace(0, r_max, 500)

        params = {
            "tsr": 7,
            "ct": 0.9,
            "TI": 0.1,
        }

        boundary_models = {
            "none": BoundaryCondition.none(),
            "madsen": BoundaryCondition.madsen(),
            "IEC": BoundaryCondition.IEC(),
            "keck": BoundaryCondition.keck(),
        }

        ########## Plot change in radial position as function of initial radial position ##########
        axial_induction_model = AxialInductionModel.Joukowsky(**params)
        r, a = axial_induction_model()

        plt.figure()
        plt.xlabel("$r/R$")
        plt.ylabel("$(\\tilde{r}-r)/R$")
        for name, model in boundary_models.items():
            r_exp = model._expand_radius(r, a)
            plt.plot(r, r_exp - r, label=name)
        plt.grid()
        plt.legend()

        plt.savefig(fig_dir / "006_boundary_condition_models.png", dpi=200, bbox_inches="tight")

        ########## Plot bondary condition ##########
        plt.figure()
        plt.xlabel("$r/R$")
        plt.ylabel("$U(0, r)$")
        for name, model in boundary_models.items():
            r_exp = model._expand_radius(r, a)
            a_exp = model._axial_velocity(r, a)
            plt.plot(r_exp, a_exp, label=name)
        plt.grid()
        plt.legend()

        plt.savefig(fig_dir / "006_boundary_conditions.png", dpi=200, bbox_inches="tight")
        ########## Plot wake evolution for varying expansion models ##########
        dwms = [Wake.StaticWake(boundary_model=x, **params) for x in boundary_models.keys()]
        res = [x.solve(x_max=15, r_max=r_max) for x in dwms]

        N = len(dwms)

        fig, axes = plt.subplots(1, N, sharey=True)
        axes[0].set_ylabel("$x/R$")
        for i, ((x, y, U, V, widths, dUdr), dwm) in enumerate(zip(res, dwms)):
            axes[i].set_title(list(boundary_models.keys())[i])
            axes[i].imshow(
                U,
                extent=[min(x), max(x), min(y), max(y)],
                origin="lower",
                interpolation="bilinear",
            )
            axes[i].imshow(
                U,
                extent=[min(x), -max(x), min(y), max(y)],
                origin="lower",
                interpolation="bilinear",
            )

            axes[i].plot(widths, y, "r--", lw=0.5)
            axes[i].plot(-widths, y, "r--", lw=0.5)

            axes[i].xaxis.set_ticks(np.arange(-r_max, r_max + 0.1, 1))
            axes[i].grid()
            axes[i].set_xlim(-r_max, r_max)
            axes[i].set_xlabel("$r/R$")

        plt.tight_layout()
        plt.savefig(
            fig_dir / "006_boundary_condition_models_wake.png", dpi=200, bbox_inches="tight"
        )

        plt.show()


main()
