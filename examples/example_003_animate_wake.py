"""
Author: Jaime Liew (jyli@dtu.dk)
date: 23-10-2019

This script shows the effect of changing various DWM input parameters on the
wake profile. The turbulence intensity (TI), tip speed ratio (tsr), actuator
disk root thickness (delta), and thrust coefficient (ct) are varied, and the
results are animated and saved as a gif.

This script requires moviepy, which can be installed using pip:

pip install moviepy

"""
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
from jDWM.Wake import StaticWake

from moviepy import VideoClip

fig_dir = Path(__file__).parent.absolute().parent / "fig"


# Copied from MoviePy 1, because it has been removed in version 2.
# https://github.com/Zulko/moviepy/blob/db19920764b5cb1d8aa6863019544fd8ae0d3cce/moviepy/video/io/bindings.py#L18C1-L32C32
# See also https://github.com/Zulko/moviepy/issues/2297
# With some updates to Matplotlib 3.8.
def mplfig_to_npimage(fig):
    """ Converts a matplotlib figure to a RGB frame after updating the canvas"""
    from matplotlib.backends.backend_agg import FigureCanvasAgg
    canvas = FigureCanvasAgg(fig)
    canvas.draw() # update/draw the elements

    # get the width and the height to resize the matrix
    l,b,w,h = canvas.figure.bbox.bounds
    w, h = int(w), int(h)

    #  exports the canvas to a memory view and then to a numpy nd.array
    mem_view = canvas.buffer_rgba()  # Update to Matplotlib 3.8
    image = np.asarray(mem_view)
    return image[:, :, :3]  # Return only RGB, not alpha.


def plot_wake(x, y, U, widths, title=""):
    # plot axial induction distribution
    fig = plt.figure(dpi=100)
    plt.title(title)
    plt.imshow(
        U.T,
        extent=[min(x), max(x), min(y), max(y)],
        origin="lower",
        interpolation="bilinear",
        vmin=0.2,
        vmax=1,
    )
    plt.imshow(
        U.T,
        extent=[min(x), max(x), min(y), -max(y)],
        origin="lower",
        interpolation="bilinear",
        vmin=0.2,
        vmax=1,
    )

    plt.plot(x, widths, "r--", lw=0.5)
    plt.plot(x, -widths, "r--", lw=0.5)

    plt.xlabel("$x/R$")
    plt.ylabel("$r/R$")

    plt.tight_layout()

    return fig


default_params = {
    "TI": 0.1,
    "ct": 0.5,
    "delta": 0.2,
    "tsr": 7,
}

animate_range = {
    "TI": (0, 0.30),
    "ct": (0.01, 1),
    "delta": (0.01, 0.5),
    "tsr": (1, 15),
}

fig_title = {
    "TI": "$TI$",
    "ct": "$C_T$",
    "delta": r"$\delta$",
    "tsr": r"$\lambda$",
}


def main():
    if __name__ == "__main__":
        dwm = StaticWake(**default_params)

        for key, ani_range in animate_range.items():
            dwm.update(**default_params)

            def make_frame(t):
                value = ani_range[0] + ani_range[1] / 5 * t
                y, x, U, V, widths, dUdr = dwm.solve(**{key: value})

                fig = plot_wake(x, y, U, widths, title=f"{fig_title[key]}={value:2.2f}")

                return mplfig_to_npimage(fig)

            animation = VideoClip(make_frame, duration=5)
            animation.write_gif(fig_dir / f"003_{key}.gif", fps=5)


main()
