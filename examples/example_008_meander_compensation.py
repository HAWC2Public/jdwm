from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
from jDWM.Wake import StaticWake

fig_dir = Path(__file__).parent.absolute().parent / "fig"


params = {
    "tsr": 7,
    "ct": 0.9,
    "TI": 0.1,
    "U": 10,
    "D_rot": 100,
}


def plot(y, x, U, V, widths):
    # plot axial induction distribution
    fig = plt.figure(dpi=200)
    plt.title("Longitudinal wind speed")
    plt.imshow(
        U.T,
        extent=[min(x), max(x), min(y), max(y)],
        origin="lower",
        interpolation="bilinear",
    )
    plt.imshow(
        U.T,
        extent=[min(x), max(x), min(y), -max(y)],
        origin="lower",
        interpolation="bilinear",
    )

    plt.plot(x, widths, "r--", lw=0.5)
    plt.plot(x, -widths, "r--", lw=0.5)

    plt.xlabel("$x/R$")
    plt.ylabel("$r/R$")

    plt.tight_layout()

    # plot radial induction distribution
    fig = plt.figure(dpi=200)
    plt.title("Lateral wind speed")
    plt.imshow(
        V.T,
        extent=[min(x), max(x), min(y), max(y)],
        origin="lower",
        interpolation="bilinear",
    )
    plt.imshow(
        V.T,
        extent=[min(x), max(x), min(y), -max(y)],
        origin="lower",
        interpolation="bilinear",
    )

    plt.plot(x, widths, "r--", lw=0.5)
    plt.plot(x, -widths, "r--", lw=0.5)

    plt.xlabel("$x/R$")
    plt.ylabel("$r/R$")

    plt.tight_layout()


def main():

    if __name__ == "__main__":
        dwm = StaticWake(
            axial_induction_model="Constant",
            meandercompensator="Reinwardt",
            viscosity_model="madsen",  # Same as HAWC2
            boundary_model="madsen",  # Same as HAWC2
            **params,
        )
        r, x, U, V, widths, dUdr = dwm.solve(
            Nx=101,  # Longitudinal discretisations
            x_max=10,  # Max downstream distance to compute (in rotor radii)
            Nr=501,  # Radial discretisations
            r_max=3,  # Max radial distance to compute (in rotor radii)
            ct=0.6,  # Thrust coefficient of wake-producing turbine
            TI=0.1,  # Ambient/meandering turbulence intensity
            U=10,  # Ambient wind speed (in m/s) (required for meandering compensation)
            D_rot=180,  # Rotor diameter of turbine (in m) (required for meandering compensation)
        )

        # r (1D array) radial positions (nondimensional with rotor radius)
        # x (1D array) longitudinal positions (nondimensional with rotor radius)
        # U (2D array) Longitudinal wind speeds (nondimensional with ambient windspeed)
        # V (2D array) radial wind speeds (nondimensional with ambient windspeed)
        # widths (1D array) Width of wake in radial direction (nondimensional with rotor radius)

        plot(r, x, U, V, widths)
        plt.savefig(fig_dir / "008_meander_compensation.png", dpi=200, bbox_inches="tight")
        plt.show()


main()
