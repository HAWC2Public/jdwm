from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
from jDWM import AxialInductionModel
from jDWM.utilities import rotor_area_mean

fig_dir = Path(__file__).parent.absolute().parent / "fig"


def main():
    if __name__ == '__main__':

        params = {
            "tsr": 7,
            "ct": 0.5,
            "TI": 0.1,
            "a_target": 0.02,
        }
        induction_models = {
            "Constant ": AxialInductionModel.Constant(**params),
            "Joukowsky ": AxialInductionModel.Joukowsky(**params),
            "thrustmatch": AxialInductionModel.ThrustMatch(**params),
            "InductionMatch": AxialInductionModel.InductionMatch(**params),
            "InductionMatch2": AxialInductionModel.InductionMatch2(**params),
            "UserInput": AxialInductionModel.UserInput(axial_r=[0, 1, 2, 3], axial_a=[0.03, 0.03, 0, 0])
        }

        plt.figure()
        for name, model in induction_models.items():
            r, a = model(ct=0.1)

            plt.plot(r, a, label=name)
        plt.legend()
        plt.savefig(fig_dir / "007_compare_axial_induction.png", dpi=200, bbox_inches="tight")
        plt.show()


main()
